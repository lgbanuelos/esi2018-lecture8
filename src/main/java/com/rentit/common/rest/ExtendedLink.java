package com.rentit.common.rest;

import org.eclipse.jetty.http.HttpMethod;
import org.springframework.hateoas.Link;

public class ExtendedLink extends Link {
    private HttpMethod method;
    public ExtendedLink(String href, String rel, HttpMethod method){
        super(href, rel);
        this.method = method;
    }
    public HttpMethod getMethod(){
        return method;
    }
}
