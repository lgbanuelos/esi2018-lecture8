package com.rentit.sales.rest.dto;

import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.sales.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.core.Relation;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@Relation(value="order", collectionRelation="orders")
public class PurchaseOrderDTO {
    Long _id;
    Resource<PlantInventoryEntryDTO> plant;
    BusinessPeriodDTO rentalPeriod;

    @Column(precision = 8, scale = 2)
    BigDecimal total;
    POStatus status;
}
