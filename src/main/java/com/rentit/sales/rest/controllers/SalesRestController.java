package com.rentit.sales.rest.controllers;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.rest.dto.PlantInventoryItemDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.sales.rest.dto.POExtensionDTO;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import com.rentit.sales.rest.assemblers.POExtensionAssembler;
import com.rentit.sales.rest.assemblers.PurchaseOrderAssembler;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/sales")
@CrossOrigin
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    SalesService salesService;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    POExtensionAssembler poExtensionAssembler;


    @GetMapping("/plants")
    public Resources<?> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return plantInventoryEntryAssembler.toResources(inventoryService.findAvailable(plantName, startDate, endDate));
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Resource<PurchaseOrderDTO> fetchPurchaseOrder(@PathVariable("id") Long id) {
        return purchaseOrderAssembler.toResource(salesService.getPurchaseOrder(1l));
    }

    @DeleteMapping("/orders/{id}")
    public Resource<PurchaseOrderDTO> handleDeleteOnPurchaseOrder(@PathVariable("id") Long id) {
        PurchaseOrder order = salesService.getPurchaseOrder(1l);
        if (order.getStatus() == POStatus.PENDING) {
            order = salesService.rejectPurchaseOrder(order);
        } else if (order.getStatus() == POStatus.OPEN) {
            order = salesService.closePurcheOrder(order);
        }
        return purchaseOrderAssembler.toResource(order);
    }

    @GetMapping("/orders")
    public Resources<?> getAllPurchaseOrders() {
        return purchaseOrderAssembler.toResources(salesService.getAllPurchaseOrders());
    }

    @PostMapping("/orders")
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) {
        PlantInventoryEntryDTO plant = partialPODTO.getPlant().getContent();
        PurchaseOrder newlyCreatedPO = salesService.createPurchaseOrder(plant.get_id(), partialPODTO.getRentalPeriod().getStartDate(), partialPODTO.getRentalPeriod().getEndDate());

        Resource<PurchaseOrderDTO> resource = purchaseOrderAssembler.toResource(newlyCreatedPO);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", resource.getRequiredLink("self").getHref());

        return new ResponseEntity<>(
                resource,
                headers, HttpStatus.CREATED);
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<?> resubmitPurchaseOrder(@PathVariable("id") Long id, @RequestBody PurchaseOrderDTO order) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/orders/{id}/allocation")
    public Resource<?> allocatePlant(@PathVariable("id") Long id) {
        return purchaseOrderAssembler.toResource(salesService.allocatePlantToPurchaseOrder(id));
    }

    @DeleteMapping("/orders/{id}/allocation")
    public Resource<?> rejectPurchaseOrder(@PathVariable("id") Long id) {
        PurchaseOrder order = salesService.findPurchaseOrder(id);
        return purchaseOrderAssembler.toResource(salesService.rejectPurchaseOrder(order));
    }

    @GetMapping("/orders/{id}/extensions")
    public Resources<Resource<POExtensionDTO>> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) {
        PurchaseOrder order = salesService.findPurchaseOrder(id);
        order.getExtensions();
        return poExtensionAssembler.toResources(order.getExtensions(), order);
    }

    @PostMapping("/orders/{id}/extensions")
    public Resource<?> requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) {
        PurchaseOrder order = salesService.requestPurchaseOrderExtension(id, extension.getEndDate());
        return purchaseOrderAssembler.toResource(order);
    }

    @PatchMapping("/orders/{id}/extensions")
    public Resource<?> acceptPurchaseOrderExtension(@PathVariable("id")Long id, PlantInventoryItemDTO plant) {
        return null;
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handlePlantNotFoundException(PlantNotFoundException ex) {
        // Code To handle Exception
    }
}
