package com.rentit.sales.application.services;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.sales.domain.model.POExtension;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    public PurchaseOrder findPurchaseOrder(Long id) {
        return purchaseOrderRepository.getOne(id);
    }

    public PurchaseOrder createPurchaseOrder(Long plantId, LocalDate startDate, LocalDate endDate) {
        PlantInventoryEntry plant = plantInventoryEntryRepository.getOne(plantId);
        PurchaseOrder order = PurchaseOrder.of(
                plant,
                BusinessPeriod.of(startDate, endDate));
        // Validate PO
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder requestPurchaseOrderExtension(Long id, LocalDate endDate) {
        PurchaseOrder order = purchaseOrderRepository.getOne(id);
//        if (order == null)
//            throw new Exception("Purchase order not found");
        order.requestExtension(endDate);
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder allocatePlantToPurchaseOrder(Long id) {
        PurchaseOrder order = purchaseOrderRepository.getOne(id);
        LocalDate startDate = order.getRentalPeriod().getStartDate();
        LocalDate endDate = order.getRentalPeriod().getEndDate();

        List<PlantInventoryItem> items = inventoryRepository.findAvailableItems(
                order.getPlant(), startDate, endDate);
        if (!items.isEmpty()) {
            PlantReservation reservation = new PlantReservation();
            reservation.setPlant(items.get(0));
            reservation.setSchedule(BusinessPeriod.of(startDate, endDate));
            plantReservationRepository.save(reservation);

            order.registerFirstAllocation(reservation);
            // Validate PO
        } else
            order.reject();

        purchaseOrderRepository.save(order);

        return order;
    }

    public PurchaseOrder rejectPurchaseOrder(PurchaseOrder order) {
        order.reject();
        purchaseOrderRepository.save(order);
        return order;
    }
    public PurchaseOrder closePurcheOrder(PurchaseOrder order) {
        order.close();
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder getPurchaseOrder(long id) {
        return purchaseOrderRepository.getOne(id);
    }

    public List<PurchaseOrder> getAllPurchaseOrders() {
        return purchaseOrderRepository.findAll();
    }
}
